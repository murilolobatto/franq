from django.db import models


class Vehicle(models.Model):
    type = models.TextField()
    color = models.TextField()
    year = models.IntegerField()
    model = models.TextField()

    def __str__(self):
        return self.model


class Garage(models.Model):
    owner_id = models.IntegerField()
    description = models.TextField()
    vehicles = models.ManyToManyField(Vehicle)

    def __str__(self):
        return self.description
