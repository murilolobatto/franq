from rest_framework import serializers

from garage.models import Vehicle, Garage


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = ['type', 'color', 'year', 'model']


class GarageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garage
        fields = ['owner_id', 'description', 'vehicles']


class CreateGarageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garage
        fields = ['owner_id', 'description']
