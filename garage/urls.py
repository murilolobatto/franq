from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    path('vehicle/', views.VehicleList.as_view()),
    path('vehicle/<int:pk>/', views.VehicleDetail.as_view()),
    path('garage/', views.GarageList.as_view()),
    path('garage/<int:pk>/', views.GarageDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)