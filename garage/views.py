from garage.models import Vehicle, Garage
from garage.serializers import VehicleSerializer, GarageSerializer, CreateGarageSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions


class VehicleList(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    """
    List all Vehicles, or create a new vehicle.
    """

    def get(self, request, format=None):
        people = Vehicle.objects.all()
        serializer = VehicleSerializer(people, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = VehicleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class VehicleDetail(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    """
    Retrieve, update or delete a vehicle instance.
    """

    def get_object(self, pk):
        try:
            return Vehicle.objects.get(pk=pk)
        except Vehicle.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        person = self.get_object(pk)
        serializer = VehicleSerializer(person)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        person = self.get_object(pk)
        serializer = VehicleSerializer(person, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        person = self.get_object(pk)
        person.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class GarageList(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    """
    List all garages, or create a new garage.
    """

    def get(self, request, format=None):
        people = Garage.objects.all()
        serializer = GarageSerializer(people, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = CreateGarageSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class GarageDetail(APIView):
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    """
    Retrieve, update or delete a garage instance.
    """

    def get_object(self, pk):
        try:
            return Garage.objects.get(pk=pk)
        except Garage.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        person = self.get_object(pk)
        serializer = GarageSerializer(person)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        person = self.get_object(pk)
        serializer = GarageSerializer(person, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        person = self.get_object(pk)
        person.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
