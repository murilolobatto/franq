from person.models import Person
from django.db.models.signals import post_save
from django.dispatch import receiver
import requests


@receiver(post_save, sender=Person)
def create_garage(sender, instance, created, **kwargs):
    if created:
        payload = {
            "owner_id": instance.id,
            "description": "Garage automatically generated for person id {}".format(instance.id)
        }

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json"
        }

        requests.post("http://localhost:8000/garage/", json=payload, headers=headers, auth=('serviceAcc', 'serviceAcc'))
